'''
Created on Sep 8, 2016

@author: whitesabertooth
'''
#################################
#  I M P O R T S
#################################
import vizdoom
import skimage.color, skimage.transform
import numpy as np
import itertools as it
import pickle
import time

learning_steps_per_epoch = 100 #Number of time steps to advance
epochs = 20  # Number of episodes to be played
frame_repeat = 12 # Number of frames to step over to be realistic
config_file_path = "config/simpler_basic.cfg"
resolution = (30, 45)  #Size to reduce the screen to
test_episodes_per_epoch = 100 #

# Converts and downsamples the input image
def preprocess(img):
    img = img[0]
    img = skimage.transform.resize(img, resolution)
    img = img.astype(np.float32)
    return img



def perform_learning_step(game):
    #READ SENSOR INPUT
    s1 = preprocess(game.get_state().image_buffer)
    #DETERMINE ACTION
    a = 1 #DEBUG
    #MAKE ACTION - Get Reward
    reward = game.make_action(actions[a], frame_repeat)
    
    #Determine the new state (i.e. terminal or new sensor input)
    isterminal = game.is_episode_finished()
    if isterminal:
        s2 = None
    else:
        s2 = preprocess(game.get_state().image_buffer)
        
    #LEARN FROM TRANSITION



# Creates and initializes ViZDoom environment.
def initialize_vizdoom(config_file_path):
    print("Initializing doom...")
    game = vizdoom.DoomGame()
    game.load_config(config_file_path)
    game.set_window_visible(False)
    game.set_mode(vizdoom.Mode.PLAYER)
    game.init()
    print("Doom initialized.")
    return game

# Create Doom instance
game = initialize_vizdoom(config_file_path)

# Action = which buttons are pressed
n = game.get_available_buttons_size()
actions = [list(a) for a in it.product([0, 1], repeat=n)]


print("Starting the training!")

time_start = time()
for epoch in range(epochs):
    print("\nEpoch %d\n-------" % (epoch + 1))
    train_episodes_finished = 0
    train_scores = []

    print("Training...")
    game.new_episode()
    for learning_step in range(learning_steps_per_epoch):
        #perform_learning_step(epoch)
        if game.is_episode_finished():
            score = game.get_total_reward()
            train_scores.append(score)
            game.new_episode()
            train_episodes_finished += 1

    print("%d training episodes played." % train_episodes_finished)

    train_scores = np.array(train_scores)

    print("Results: mean: %.1f±%.1f," % (train_scores.mean(), train_scores.std()), \
        "min: %.1f," % train_scores.min(), "max: %.1f," % train_scores.max())

    print("\nTesting...")
    test_episode = []
    test_scores = []
    for test_episode in range(test_episodes_per_epoch):
        game.new_episode()
        while not game.is_episode_finished():
            state = preprocess(game.get_state().image_buffer)
            #best_action_index = get_best_action(state)
            best_action_index = 1 #Debug line

            game.make_action(actions[best_action_index], frame_repeat)
        r = game.get_total_reward()
        test_scores.append(r)

    test_scores = np.array(test_scores)
    print("Results: mean: %.1f±%.1f," % (
    test_scores.mean(), test_scores.std()), "min: %.1f" % test_scores.min(), "max: %.1f" % test_scores.max())

    print("Saving the network weigths...")
    #pickle.dump(get_all_param_values(net), open('weights.dump', "w"))
    
    print("Total elapsed time: %.2f minutes" % ((time() - time_start) / 60.0))


#Finished Learning
game.close()
print("======================================")
print("Training finished. It's time to watch!")

# Load the network's parameters from a file
#params = pickle.load(open('weights.dump', "r"))
#set_all_param_values(net, params) #Loads the neural network parameters

def watchgame(game):
    # Reinitialize the game with window visible
    game.set_window_visible(True)
    game.set_mode(vizdoom.Mode.ASYNC_PLAYER)
    game.init()
    
    episodes_to_watch = 10
    for i in range(episodes_to_watch):
        game.new_episode()
        while not game.is_episode_finished():
            state = preprocess(game.get_state().image_buffer)
            #best_action_index = get_best_action(state)
            best_action_index = 1 #Debug line
            
            # Instead of make_action(a, frame_repeat) in order to make the animation smooth
            game.set_action(actions[best_action_index])
            for ii in range(frame_repeat):
                game.advance_action()
    
        # Sleep between episodes
        time.sleep(1.0)
        score = game.get_total_reward()
        print("Total score: ", score)

watchgame(game)

if __name__ == '__main__':
    pass