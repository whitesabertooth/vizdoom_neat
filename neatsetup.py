'''
Created on Sep 13, 2016

@author: whitesabertooth
'''

import MultiNEAT as NEAT
from MultiNEAT import EvaluateGenomeList_Serial
from MultiNEAT import EvaluateGenomeList_Parallel
from MultiNEAT import GetGenomeList, ZipFitness
from concurrent.futures import ProcessPoolExecutor, as_completed
import copy
import numpy as np
import sys
import os
import shutil
NEAT_types = ['ESHyperNEAT','NEAT']
GLOBAL_NEAT = 'NEAT'

class GenomeRun(object):
    '''
    class docs
    '''
    def __init__(self,genome=None,filename=None,substrate=None,params=None):
        #self.genome = genome
        self.net = NEAT.NeuralNetwork()
        self.genomeid = None
        self.params = params
        if genome:
            self.genomeid = copy.copy(genome.GetID())#save for later
            if substrate == None:
                genome.BuildPhenotype(self.net)
                genome.CalculateDepth()
                self.depth = genome.GetDepth()
            else:
                genome.BuildESHyperNEATPhenotype(self.net,substrate,params)
                genome.CalculateDepth()
                self.depth = genome.GetDepth()
        elif filename:
            self.net.Load(filename)
    def testState(self,state):
        self.net.Input(state)
        for _ in range(self.depth): #activate how many times it is deep
            self.net.Activate()
        #self.net.ActivateFast() #Assumes Activation unsigned sigmoid everywhere
        newarr = []
        for i in self.net.Output():
            if i == 1.0:
                newarr.append(1)
            else:
                newarr.append(0)
        return newarr
    def getPopulationNumber(self):
        if self.genomeid:
            return self.genomeid
        else:
            return -1
    def save(self,filename):
        self.net.Save(filename)
    def update(self):
        #Perform a weight update using hebbian learning (rate is 0.3 hard coded in MultiNEAT code)
        sys.stdout.write("A")
        self.net.Adapt(self.params)

class MyNEAT(object):
    '''
    classdocs
    '''


    def __init__(self, params, evaluation_fcn, initgenome=None, initpop=None):
        '''
        Constructor
        '''
        self.params = copy.copy(params)
        self.type = self.params['type'] #'NEAT','ESHyperNEAT'
        self.evaluate = evaluation_fcn
        self.setupNEATparams()
        self.initgemone = initgenome
        self.initpop = initpop
        self.substrate = None
        self.seed = np.random.randint(0,10000)  #this is a random start seed


    def setupESHyperNEATparams(self):
        params = NEAT.Parameters()
        #params.PopulationSize = 200;
        params.PopulationSize = self.params['population_size']

        params.DynamicCompatibility = True;
        params.CompatTreshold = 2.0;
        params.YoungAgeTreshold = 15;
        params.SpeciesMaxStagnation = 100;
        params.OldAgeTreshold = 35;
        params.MinSpecies = 5;
        params.MaxSpecies = 10;
        params.RouletteWheelSelection = False;

        params.MutateRemLinkProb = 0.02;
        params.RecurrentProb = 0;#no recurrent
        params.OverallMutationRate = 0.15;
        params.MutateAddLinkProb = 0.08;
        params.MutateAddNeuronProb = 0.01;
        params.MutateWeightsProb = 0.90;
        params.MaxWeight = 300.0;#is a +/- max - the inputs are 0-255, default 8.0
        params.WeightMutationMaxPower = 0.2;
        params.WeightReplacementMaxPower = 1.0;

        params.MutateActivationAProb = 0.0;
        params.ActivationAMutationMaxPower = 0.5;
        params.MinActivationA = 0.05;
        params.MaxActivationA = 6.0;

        params.MutateNeuronActivationTypeProb = 0.03;

        params.ActivationFunction_SignedSigmoid_Prob = 0.0;
        params.ActivationFunction_UnsignedSigmoid_Prob = 0.0;
        params.ActivationFunction_Tanh_Prob = 1.0;
        params.ActivationFunction_TanhCubic_Prob = 0.0;
        params.ActivationFunction_SignedStep_Prob = 1.0;
        params.ActivationFunction_UnsignedStep_Prob = 0.0;
        params.ActivationFunction_SignedGauss_Prob = 1.0;
        params.ActivationFunction_UnsignedGauss_Prob = 0.0;
        params.ActivationFunction_Abs_Prob = 0.0;
        params.ActivationFunction_SignedSine_Prob = 1.0;
        params.ActivationFunction_UnsignedSine_Prob = 0.0;
        params.ActivationFunction_Linear_Prob = 1.0;

        params.DivisionThreshold = 0.5;
        params.VarianceThreshold = 0.03;
        params.BandThreshold = 0.3;
        params.InitialDepth = 2;#starts with no hidden layer
        params.MaxDepth = 3;#only one hidden layer
        params.IterationLevel = 1;
        params.Leo = False;
        params.GeometrySeed = False;
        params.LeoSeed = False;
        params.LeoThreshold = 0.3;
        params.CPPN_Bias = -1.0;
        params.Qtree_X = 0.0;
        params.Qtree_Y = 0.0;
        params.Width = 1.;
        params.Height = 1.;
        params.Elitism = 0.1;

        rng = NEAT.RNG()
        rng.TimeSeed()
        self.neatparams = params

    def createSubstrate(self):
        #Input []  dim=(screen dimmentions, i.e. the input size:30x45=1350)x2  the 2 comes from x,y position of pixel
        #Hidden [] empty since there is no relations
        #Output [] dim=(#actions)x2  2 because needs to be the same size as input; and break it into (movement,action):
        #    movement: -2 turn left; -1 move left; +1 move right; +2 turn right
        #    action:   0 shoot, 1 move forward, -1 move backward, 0.# change weapon #
        #    Currently: actions are: (left,right,shoot)

        #Create input visual map (30x45)x2
        visualmap = []
        for xx in range(self.params['resolution'][0]):
            for yy in range(self.params['resolution'][1]):
                visualmap.append((xx,yy))

        actionmap = [(-1., 0.),(1., 0.),(0., 0.)] #(left, right, shoot)
        substrate = NEAT.Substrate(visualmap,
                                   [],
                                   actionmap)

        substrate.m_allow_input_hidden_links = False;
        substrate.m_allow_input_output_links = False;
        substrate.m_allow_hidden_hidden_links = False;
        substrate.m_allow_hidden_output_links = False;
        substrate.m_allow_output_hidden_links = False;
        substrate.m_allow_output_output_links = False;
        substrate.m_allow_looped_hidden_links = False;
        substrate.m_allow_looped_output_links = False;

        substrate.m_allow_input_hidden_links = True;
        substrate.m_allow_input_output_links = False;
        substrate.m_allow_hidden_output_links = True;
        substrate.m_allow_hidden_hidden_links = False;

        substrate.m_hidden_nodes_activation = NEAT.ActivationFunction.SIGNED_SIGMOID;
        substrate.m_output_nodes_activation = NEAT.ActivationFunction.UNSIGNED_SIGMOID;

        substrate.m_with_distance = False;

        substrate.m_max_weight_and_bias = 8.0;
        return substrate

    def setupNEATparams(self):
        if self.type == 'NEAT':
            self.setupNEATparams2()
        elif self.type == 'ESHyperNEAT':
            print("Using ESHyperNEAT")
            self.setupESHyperNEATparams()

    def setupNEATparams2(self):
        params = NEAT.Parameters()
        #params.PopulationSize = 150
        params.PopulationSize = self.params['population_size']
        params.DynamicCompatibility = True
        params.WeightDiffCoeff = 4.0
        params.CompatTreshold = 2.0
        params.YoungAgeTreshold = 15
        params.SpeciesMaxStagnation = 15
        params.OldAgeTreshold = 35
        params.MinSpecies = 5
        params.MaxSpecies = 10
        params.RouletteWheelSelection = False
        params.RecurrentProb = 0.0 #Make it fully recurrent
        params.OverallMutationRate = 0.8

        params.MutateWeightsProb = 0.90

        params.WeightMutationMaxPower = 2.5
        params.WeightReplacementMaxPower = 5.0
        params.MutateWeightsSevereProb = 0.5
        params.WeightMutationRate = 0.25

        #params.MaxWeight = 8
        params.MaxWeight = 11111300.0;#is a +/- max - the inputs are 0-255, default 8.0

        params.MutateAddNeuronProb = 0.03
        params.MutateAddLinkProb = 0.05
        params.MutateRemLinkProb = 0.0

        params.MinActivationA  = 4.9
        params.MaxActivationA  = 4.9

        params.ActivationFunction_SignedSigmoid_Prob = 0.0
        params.ActivationFunction_UnsignedSigmoid_Prob = 1.0
        params.ActivationFunction_Tanh_Prob = 0.0
        params.ActivationFunction_SignedStep_Prob = 0.0

        params.CrossoverRate = 0.75  # mutate only 0.25
        params.MultipointCrossoverRate = 0.4
        params.SurvivalRate = 0.2
        self.neatparams = params


    def train(self):
        bestall = {'score':-100000,'genome':None,'pop':None}
        print("Totals: Runs=%d, Generations=%d, Population=%d" % (self.params['num_trainings'],self.params['num_generations'],self.params['population_size']))
        for run in range(self.params['num_trainings']):#Train 100 population groups through 1000 generations
            print("Executing run %d..." % (run+1))
            data = self.getbest(run)
            print('Run: %d: Best score: %f [' % (run+1,data['score']),data['fitness_list'],']')
            print("")
            #Use only the best run
            if data['score'] > bestall['score']:
                bestall = data
                bestall['run'] = run

        print("-"*40)
        print('Best score was run %d: %.4f' %(bestall['run']+1,bestall['score']))

        return bestall

    def createNN(self,genome):
        if self.type == 'ESHyperNEAT':
            net = GenomeRun(genome,substrate=self.createSubstrate(),params=self.neatparams)
        else:
            net = GenomeRun(genome,params=self.neatparams)#give params for learning NN
        return net

    def internal_evaluatate(self,genome):
        if self.params['learn']:
            genome.Save('tmp/tmp%d_before.genome'%(genome.GetID()))
        net = self.createNN(genome)
        output = self.evaluate(net,self.seed,self.params['learn'],self.dorecord)
        if self.params['learn']:
            genome.DerivePhenotypicChanges(net.net) #update genome based on learned weights during execution
            genome.Save('tmp/tmp%d_after.genome'%(genome.GetID()))
        return output

    def selftest(self):
        g = NEAT.Genome(0, self.params['num_inputs']+1, 0, self.params['num_outputs'], False, NEAT.ActivationFunction.UNSIGNED_SIGMOID, NEAT.ActivationFunction.UNSIGNED_SIGMOID, 0, self.neatparams)
        pop = NEAT.Population(g, self.neatparams, True, 1.0, 1)
        pop.RNG.Seed(1)
        genome_list = NEAT.GetGenomeList(pop)
        for genome in genome_list:
            net = GenomeRun(genome)
            tmp1file = 'tmp1.net'
            net.save(tmp1file)
            net = GenomeRun(genome)
            tmp2file = 'tmp2.net'
            net.save(tmp2file)
            #Run command line diff tool and look for no output
            import subprocess
            pp = subprocess.Popen(['diff',tmp1file,tmp2file],stdout=subprocess.PIPE)
            out,err = pp.communicate()
            if out != b'':
                print("%d There is a difference"%(genome.GetID()))
            else:
                print("%d No difference"%(genome.GetID()))
    def getbest(self,i):
        '''
            Run the Epochs (or generations) of one set of population
            Returns {'score':-100000,'gen':0, 'fitness_list':[]} of the last generation with average score and best neural network / genome
        '''
        #########################
        # Initialize population

        #Select to start a specific genome
        if self.initgemone:
            #continuing
            g = self.initgemone
            print("using loaded genome")
        else:
            substrate = self.createSubstrate()
            #Always need an extra input (the bias)
            if self.type == 'ESHyperNEAT':
                g = NEAT.Genome(0,
                                substrate.GetMinCPPNInputs(),
                                0,
                                substrate.GetMinCPPNOutputs(),
                                False,
                                NEAT.ActivationFunction.TANH,
                                NEAT.ActivationFunction.TANH,
                                0,
                                self.neatparams)
            else:
                g = NEAT.Genome(0, self.params['num_inputs']+1, 0, self.params['num_outputs'], False, NEAT.ActivationFunction.UNSIGNED_SIGMOID, NEAT.ActivationFunction.UNSIGNED_SIGMOID, 0, self.neatparams)

        #Select to start a specific population
        if self.initpop:
            pop = self.initpop
            print("using loaded population")
        else:
            pop = NEAT.Population(g, self.neatparams, True, 1.0, i)
            pop.RNG.Seed(i)

        #Verify num_generations is greater than 0
        if self.params['num_generations'] < 1:
            print("num_generations must be greater than 1")
            exit(1)
        self.dorecord = False
        RECORD_EVERY = self.params['record_every']

        #make directories to save stuff if not exist
        cpdir = self.params['prefix']+'data'
        if not os.path.exists(cpdir):
            os.mkdir(cpdir)
        #=========================================================
        # Iterate through the generations, starting from where left off and adding more
        for generation in range(self.params['lastgen'],self.params['lastgen']+self.params['num_generations']):
            #only record on last time and every so often generation
            snapshot = generation == self.params['num_generations'] -1 or (generation+1) % RECORD_EVERY == 0
            if snapshot:
                self.dorecord = True
            else:
                self.dorecord = False

            sys.stdout.write("  Executing generation %d..." % (generation+1))
            sys.stdout.flush()
            ##########################################
            # Select genomes from population, and perform evaluation to get fitness value
            # This evaluation is run in parallel
            genome_list = NEAT.GetGenomeList(pop)
            fitness_list = EvaluateGenomeList_Serial(genome_list, self.internal_evaluatate, display=False)
            #fitness_list = EvaluateGenomeList_Parallel(genome_list, self.evaluate, display=False, cores=2)


            #Make best NN again (need to do this before set evaluated)
            if snapshot:
                #last generation so save
                mfit = max(fitness_list)
                midx = fitness_list.index(mfit)
                bestnn = self.createNN(genome_list[midx])
                bestnnfile = self.params['prefix']+'tmpbest%d.net'%(bestnn.getPopulationNumber())
                bestnn.save(bestnnfile)
                bestgen = genome_list[midx]
                bestgenid = bestnn.getPopulationNumber()
                outstr = "Best Genome: score %.1f, ID %d" %(mfit,bestnn.getPopulationNumber())
                #copy replays to directory
                #Make current generation data
                finalcpdir = cpdir+"/"+str(generation+1)
                if not os.path.exists(finalcpdir):
                    os.mkdir(finalcpdir)
                #save NN
                shutil.copyfile(bestnnfile,finalcpdir+"/"+os.path.basename(bestnnfile))
                #save episode playbacks
                for ep in range(self.params['num_episodes']):
                    bestfile = 'tmp/tmpgame%d_%d.lmp' % (bestnn.getPopulationNumber(),ep)
                    shutil.copyfile(bestfile,finalcpdir+"/"+os.path.basename(bestfile))

            #Apply fitness (and set evaluated)
            NEAT.ZipFitness(genome_list, fitness_list)

            ##################
            # Print results
            fitness_data = np.array(fitness_list)
            best = fitness_data.mean()  #use mean to calculate the score of the last generation
            #print("    Reward: mean: %.1f±%.1f," % (
            #test_scores.mean(), test_scores.std()), "min: %.1f" % test_scores.min(), "max: %.1f" % test_scores.max())

            sys.stdout.write("mean: %.1f±%.1f (%.1f to %.1f) "%(fitness_data.mean(),fitness_data.std(),fitness_data.min(),fitness_data.max()))
            sys.stdout.write("Reproducing..")
            sys.stdout.flush()

            ####################
            # Evolve population
            pop.Epoch() #evolve population

            ###################
            # Temp Save incase of crash
            tmpfile = self.params['prefix']+'temppop.dump'
            if os.path.exists(tmpfile):
                os.remove(tmpfile)
            pop.Save(tmpfile)

            #save score every generation
            otherstr = "mean: %.1f±%.1f (%.1f to %.1f) "%(fitness_data.mean(),fitness_data.std(),fitness_data.min(),fitness_data.max())
            outstrfile = cpdir+"/scores.txt"
            with open(outstrfile,'a+') as f:#append or create
                f.write(otherstr+"\n")

            if snapshot:
                #this should be a continuation from above
                #save score
                outstrfile = finalcpdir+"/bestscore.txt"
                with open(outstrfile,'w') as f:
                    f.write(outstr+"\n")
                #save population
                shutil.copyfile(tmpfile,finalcpdir+"/"+os.path.basename(tmpfile))
                sys.stdout.write("copied snapshot to %s"%(finalcpdir))

            sys.stdout.write("\n")
            sys.stdout.flush()

        #==============================
        #Always save the last generation and return it
        print(outstr)
        maxbest = {}
        maxbest['score'] = best
        maxbest['gen'] = generation
        maxbest['fitness_list'] = fitness_list

        #save population
        maxbest['pop'] = pop

        #find the best score genome
        maxbest['genome'] = pop.Species[0].GetLeader()#bestgen
        maxbest['popnum'] = bestgenid
        #make neural network
        #if self.type == 'ESHyperNEAT':
        #    maxbest['net'] = GenomeRun(pop.Species[0].GetLeader(),substrate=self.createSubstrate(),params=self.neatparams)
        #else:
        #    maxbest['net'] = GenomeRun(pop.Species[0].GetLeader())
        maxbest['net'] = bestnn

        return maxbest
