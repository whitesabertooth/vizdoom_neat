'''
Created on Sep 13, 2016

@author: whitesabertooth
'''
import vizdoom
import numpy as np
import itertools as it
import skimage.color, skimage.transform
import copy
import time

class VIZDOOM(object):
    '''
    classdocs
    '''


    def __init__(self,params):
        '''
        Constructor
        '''
        self.params = copy.copy(params)
        #Set defaults if not already set
        if 'config_file_path' not in self.params:
            self.params['config_file_path'] = 'config/simpler_basic3.cfg'
        if 'frame_repeat' not in self.params:
            self.params['frame_repeat'] = 10
        if 'episodes_to_watch' not in self.params:
            self.params['episodes_to_watch'] = 10
        if 'resolution' not in self.params:
            self.params['resolution'] = (30,45)

    # Creates and initializes ViZDoom environment.
    def startgame(self,watch=False,seed=None):
        #print("Initializing doom...")
        game = vizdoom.DoomGame()
        game.load_config(self.params['config_file_path'])
        if seed:
            game.set_seed(seed) #needs to be set before init
        if watch:
            # Reinitialize the game with window visible
            game.set_window_visible(False)
            #game.set_window_visible(True)
            #game.set_mode(vizdoom.Mode.ASYNC_PLAYER)
            game.set_mode(vizdoom.Mode.PLAYER)
            #game.set_render_hud(True)
            #game.set_render_weapon(True)
        else:
            game.set_window_visible(False)
            game.set_mode(vizdoom.Mode.PLAYER)

        #game.set_living_reward(-0.2) #Cost of living is low, so 300*.2 = -60 points, but killing is 100, shooting is -6
        game.init()
        #print("Doom initialized.")
        self.game = game
        # Action = which buttons are pressed
        n = self.game.get_available_buttons_size()
        self.actions = [list(a) for a in it.product([0, 1], repeat=n)]


    def make_action(self,actionidx,realtime=False):
        if realtime:
            output = self.game.set_action(actionidx)
            for _ in range(self.params['frame_repeat']):
                self.game.advance_action(1)
        else:
            output = self.game.make_action(actionidx, self.params['frame_repeat'])
        return output


    def getState(self):
        return preprocess(self.game.get_state().image_buffer,self.params['resolution'])

    def getStateSize(self):
        # Returns the screen size in bits from visdoom X * Y * 8 bits, because it currently is GRAY8
        return self.params['resolution'][0] * self.params['resolution'][1] #* 8 TEST

def preprocess(img,resolution):
    ''' Down samples the input image and convert to 0/1 float64 1D array (needed for NN)
    '''
    img = img[0]
    img = skimage.transform.resize(img, resolution)

    #Used to suppress the warnings skimage outputs because of float to uint8 conversion
    import warnings
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        imgb = skimage.img_as_ubyte(img,force_copy=True) #Convert from floats to uint (because GRAY8 at this time)

    #newimg = np.unpackbits(imgb) #Make array of 0/1
    newimg = imgb.reshape((resolution[0]*resolution[1],)) #flatten to 1D
    return newimg.astype(np.float64)/255.0 #required float64 for NEAT #Scale so range is [0,1]

def showimg(img):
    '''Show the image in a pop-up window
    '''
    from skimage.viewer import ImageViewer
    vv = ImageViewer(img)
    vv.show()
