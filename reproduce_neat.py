'''
Created on Sep 23, 2016

@author: whitesabertooth

Runs the neatsetup.selftest() function to make sure NEAT can reproduce the same NN from the same Genome
'''

if __name__ == '__main__':
    import neatsetup
    neat_params = {}
    neat_params['num_generations'] = 1 #Number of epochs to train a population group
    neat_params['num_trainings'] = 1 #Number of population groups to train
    neat_params['population_size'] = 100 #Size of population
    neat_params['num_inputs'] = 30*45 #Set later 
    neat_params['num_outputs'] = 3 #Set later
    neat_params['prefix'] = 'tt'
    neat_params['type'] = 'NEAT'

    neat = neatsetup.MyNEAT(neat_params,None)
    neat.selftest()
    pass