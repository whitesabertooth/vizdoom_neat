'''
Created on Sep 8, 2016

@author: whitesabertooth
'''
#################################
#  I M P O R T S
#################################
import numpy as np
import pickle
import time
from vizdoomsetup import VIZDOOM
from neatsetup import MyNEAT
import sys
from neatsetup import GenomeRun
import signal


test_episodes_per_epoch = 5 #for training
episodes_to_watch = 1 #episodes to show during watching
save_best_genome_file = 'bestgen.dump'
save_best_net_file = 'bestnet.dump'
save_best_pop_file = 'bestpop.dump'
timeout_duration = 5 #seconds - vizdoom all episodes execution.  this should be way too much

vizdoom_params = {}
vizdoom_params['frame_repeat'] = 12 # Number of frames to step over to be realistic
vizdoom_params['config_file_path'] = "config/simpler_basic.cfg"
vizdoom_params['resolution'] = (30, 45)  #Size to reduce the screen to

neat_params = {}
'''
Times running doom = 
    num_generations * num_training * population_size
'''
neat_params['num_generations'] = 1 #Number of epochs to train a population group
neat_params['num_trainings'] = 1 #Number of population groups to train
neat_params['population_size'] = 1 #Size of population
neat_params['num_inputs'] = 0 #Set later 
neat_params['num_outputs'] = 0 #Set later

class TimeoutError(Exception):
    pass

def handler(signum, frame):
    raise TimeoutError("vizdoom too long")

# Create Doom instance
mygame = VIZDOOM(vizdoom_params)

def evaluate(mygenome,realtime,num_episodes):
    '''
    The root of the fitness evaluation - running multiple episodes and see the average score
    '''
    #Uses the global mygame
    #print("\nTesting...")
    test_scores = []
    runtimes = []
    fitness = -250.0 #default if crash
    signal.signal(signal.SIGALRM, handler)
    signal.alarm(timeout_duration)#turn on timeout error
    try:
        for test_episode in range(num_episodes):
            #print("      Starting episode %d..." % (test_episode+1))
            if not realtime:
                time_start = time.time()
            mygame.game.new_episode()
            while not mygame.game.is_episode_finished():
                #Note: scenario is setup in config to run 300 ticks
                try:
                    state = mygame.getState()
                    best_action_index = mygenome.testState(state)
                    #best_action_index = 1 #Debug line
    
                    mygame.make_action(best_action_index,realtime=realtime)
                except TimeoutError as e:
                    raise(e) #reraise
                    
                except Exception as e:#just ignore errors and proceed
                    print("had error in game loop:",e)
                        
            r = mygame.game.get_total_reward()
            test_scores.append(r)
            if not realtime:
                runtime = (time.time() - time_start)
                #print("      Episode %3d: reward=%f, runtime=%.2f" %(test_episode+1,r,runtime))
                runtimes.append(runtime)
            
            if realtime:
                # Sleep between episodes
                time.sleep(1.0)
                
    except TimeoutError as e:
        print("Vizdoom taking too long-restarting")
        mygame.close()
        mygame.startgame()
        #signal.alarm(0) #turn off alarm
        #raise e
    except Exception as e:#just ignore errors and proceed
        print("had error in episode:",e)
    signal.alarm(0) #turn off alarm
            

    test_scores = np.array(test_scores)
    #print("    Reward: mean: %.1f±%.1f," % (
    #test_scores.mean(), test_scores.std()), "min: %.1f" % test_scores.min(), "max: %.1f" % test_scores.max())
    if not realtime:
        runtimes = np.array(runtimes)
        #print("    Runtime (s): mean: %.1f±%.1f," % (
        #runtimes.mean(), runtimes.std()), "min: %.1f" % runtimes.min(), "max: %.1f" % runtimes.max())
        sys.stdout.write(".") #add info
        sys.stdout.flush()
    #print("") #skip line
    #Return the fitness as the mean of all episode scores
    if len(test_scores) > 0:
        fitness = test_scores.mean()
        
    return (fitness,test_scores)


def runTesting(genome):
    net = GenomeRun(genome)
    return evaluate(net,realtime=False,num_episodes=test_episodes_per_epoch)[0]



def watchgame(mygame,net):
    mygame.startgame(watch=True)
    output = evaluate(net,realtime=True,num_episodes=episodes_to_watch)
    mygame.game.close()
    return output


def doTraining(initgenome=None,initpop=None):
    mygame.startgame()
    #Set parameters based on vizdoom
    print("Starting the training!")
    neat_params['num_inputs'] = mygame.getStateSize() #Set from screen size 
    neat_params['num_outputs'] = len(mygame.actions)  #Set actions
    neat = MyNEAT(neat_params,runTesting,initgenome=initgenome,initpop=initpop)
    time_start = time.time()
    bestgen = neat.train()
    print("Total elapsed time: %.2f minutes" % ((time.time() - time_start) / 60.0))
    #Finished Learning
    mygame.game.close()    
    return bestgen

def doWatching(bestgen):
    (fitness,test_scores) = watchgame(mygame,bestgen)
    print("Final score: mean: %.1f±%.1f," % (
    test_scores.mean(), test_scores.std()), "min: %.1f" % test_scores.min(), "max: %.1f" % test_scores.max())


# Load the network's parameters from a file
#params = pickle.load(open('weights.dump', "r"))
#set_all_param_values(net, params) #Loads the neural network parameters

def saveNet(net,filename=save_best_net_file):
    sys.stdout.write("Saving the best neural network to %s..." %(filename))
    sys.stdout.flush()
    net.save(filename)
    sys.stdout.write("done\n")
    sys.stdout.flush()

def loadNet(filename=save_best_net_file):
    sys.stdout.write("Loading neural network from %s..." %(filename))
    sys.stdout.flush()
    net = GenomeRun(filename=filename)
    sys.stdout.write("done\n")
    sys.stdout.flush()
    return net

def saveGenome(net,filename=save_best_genome_file):
    sys.stdout.write("Saving the best genome to %s..." %(filename))
    sys.stdout.flush()
    net.Save(filename)
    sys.stdout.write("done\n")
    sys.stdout.flush()

def loadGenome(filename=save_best_genome_file):
    import MultiNEAT as NEAT
    sys.stdout.write("Loading genome from %s..." %(filename))
    sys.stdout.flush()
    net = NEAT.Genome(filename)
    sys.stdout.write("done\n")
    sys.stdout.flush()
    return net


def savePop(net,filename=save_best_pop_file):
    sys.stdout.write("Saving the best population to %s..." %(filename))
    sys.stdout.flush()
    net.Save(filename)
    sys.stdout.write("done\n")
    sys.stdout.flush()

def loadPop(filename=save_best_pop_file):
    import MultiNEAT as NEAT
    sys.stdout.write("Loading population from %s..." %(filename))
    sys.stdout.flush()
    net = NEAT.Population(filename)
    sys.stdout.write("done\n")
    sys.stdout.flush()
    return net
    
    

if __name__ == '__main__':
    import argparse
    #=======================
    # Process arguments
    parser = argparse.ArgumentParser(description='Demo NEAT.')
    parser.add_argument('--load_genome', dest='genome', metavar='FILE',
                        help='File of a dumped genome')
    parser.add_argument('--load_net', dest='net', metavar='FILE',
                        help='File of a dumped neural network')
    parser.add_argument('--gen', type=int, dest='num_gen', metavar='NUM',
                        help='Number of epochs/generations to train a population group.')
    parser.add_argument('--train', type=int, dest='num_train', metavar='NUM',
                        help='Number of population groups to train.')
    parser.add_argument('--pop', type=int, dest='num_pop', metavar='NUM',
                        help='Size of the population for a group.')
    parser.add_argument('--load_pop', dest='pop', metavar='FILE',
                        help='File of a dumped population')
    
    args = parser.parse_args()

    #If net set then just show demo
    if args.net:
        net = loadNet(args.net)
        print("Using neural network loaded to watch")
        doWatching(net)
        exit(0)
        
    initgenome = None
    initpop = None
    if args.genome:
        initgenome = loadGenome(args.genome)
    if args.pop:
        initpop = loadPop(args.pop)
    if args.num_gen:
        neat_params['num_generations'] = args.num_gen #Number of epochs to train a population group
    if args.num_train:
        neat_params['num_trainings'] = args.num_train #Number of population groups to train
    if args.num_pop:
        neat_params['population_size'] = args.num_pop #Size of population

    
    #=======================
    # Do training
    best = doTraining(initgenome=initgenome,initpop=initpop)
    
    #=======================
    # Save data
    print("======================================")
    import os
    if os.path.exists(save_best_net_file):
        os.remove(save_best_net_file)
    saveNet(best['net'],save_best_net_file)
    if os.path.exists(save_best_genome_file):
        os.remove(save_best_genome_file)
    saveGenome(best['genome'], save_best_genome_file)
    if os.path.exists(save_best_pop_file):
        os.remove(save_best_pop_file)
    savePop(best['pop'],save_best_pop_file)
    
    
    #=======================
    # Watch best network
    print("======================================")
    print("Training finished. It's time to watch!")
    doWatching(best['net'])
