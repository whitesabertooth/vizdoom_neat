'''
Created on Sep 8, 2016

@author: whitesabertooth
'''
#################################
#  I M P O R T S
#################################
import numpy as np
import pickle
import time
from vizdoomsetup import VIZDOOM
from neatsetup import MyNEAT,NEAT_types
import sys
from neatsetup import GenomeRun
import signal
import os

## GLOBAL CONST
CONST_VERIFY_IMAGES = False
'''ADD Progress bar
def update_progress(progress):
    print '\r[{0}] {1}%'.format('#'*(progress/10), progress)
looks like this: [ ########## ] 100%
'''
score_offset = 300 #worst possible score...this is a guess; added to score to make base of 0
VIZDOOM_SEED = 456
test_episodes_per_epoch = 2 #for training
episodes_to_watch = test_episodes_per_epoch
save_best_genome_file = 'gen.dump'
save_best_net_file = 'net.dump'
save_best_pop_file = 'pop.dump'
timeout_duration = 5 #seconds - vizdoom all episodes execution.  this should be way too much
resolution = (30, 45)

vizdoom_params = {}
vizdoom_params['frame_repeat'] = 12 # Number of frames to step over to be realistic
vizdoom_params['config_file_path'] = "config/simpler_basic3.cfg"
vizdoom_params['resolution'] = resolution  #Size to reduce the screen to

neat_params = {}
'''
Times running doom =
    num_generations * num_training * population_size
'''
neat_params['num_generations'] = 1 #Number of epochs to train a population group
neat_params['num_trainings'] = 1 #Number of population groups to train
neat_params['population_size'] = 1 #Size of population
neat_params['num_inputs'] = 0 #Set later
neat_params['num_outputs'] = 0 #Set later
neat_params['resolution'] = resolution  #for HyperNEAT need the x,y resolution
neat_params['record_every'] = 10 #how often to record playbacks and pop by generations

class TimeoutError(Exception):
    pass

def handler(signum, frame):
    raise TimeoutError("vizdoom too long")

# Create Doom instance
mygame = VIZDOOM(vizdoom_params)
allimg ={}

def evaluate(mygenome,realtime,num_episodes,seed=np.random.randint(0,10000),dolearn=False,record=False):
    '''
    The root of the fitness evaluation - running multiple episodes and see the average score
    '''
    #Uses the global mygame
    #sys.stdout.write("    Testing %d..." %(mygenome.getPopulationNumber()))
    if not realtime:
        #mygenome.save('tmp/tmp%d_before.net'%(mygenome.getPopulationNumber()))
        if CONST_VERIFY_IMAGES:
            allimg[mygenome.getPopulationNumber()] =[]
    else:
        #mygenome.save('retmp%d.net'%(mygenome.getPopulationNumber()))
        pass
    test_scores = []
    runtimes = []
    fitness = 0 #default if crash
    mygame.game.close()
    mygame.startgame(seed=VIZDOOM_SEED) #Seeds will automatically change as new episodes are called by +1 (roughly because there is another +1 added on the first new episode called)
    for test_episode in range(num_episodes):
        #print("      Starting episode %d..." % (test_episode+1))
        if not realtime:
            time_start = time.time()
        firsttime = True

        #Always record playback
        if record:
            mygame.game.new_episode('tmp/tmpgame%d_%d.lmp' %(mygenome.getPopulationNumber(),test_episode))
        else:
            mygame.game.new_episode()
        #print("ViZDoom Seed %d, DOOM Seed %d"%(mygame.game.get_seed(),mygame.game.get_doom_seed()))
        while not mygame.game.is_episode_finished():
            #Note: scenario is setup in config to run 300 ticks
            state = mygame.getState()
            if CONST_VERIFY_IMAGES:
                if not realtime and firsttime:
                    firsttime = False
                    allimg[mygenome.getPopulationNumber()].append(state)
            best_action_index = mygenome.testState(state)
            if dolearn:
                mygenome.update()
            mygame.make_action(best_action_index,realtime=False)#realtime

        #-End while retry

        #Square the score after offset [to make 0 worst score]; then add weight depending on episode
        r = ((mygame.game.get_total_reward()+score_offset)**2)# * episode_fitness_weight[test_episode] #weight the score
        test_scores.append(r)
        if not realtime:
            runtime = (time.time() - time_start)
            #print("      Episode %3d: reward=%f, runtime=%.2f" %(test_episode+1,r,runtime))
            runtimes.append(runtime)




    test_scores = np.array(test_scores)
    if not realtime:
        runtimes = np.array(runtimes)
        #print("    Runtime (s): mean: %.1f±%.1f," % (
        #runtimes.mean(), runtimes.std()), "min: %.1f" % runtimes.min(), "max: %.1f" % runtimes.max())
        sys.stdout.write(".") #add info
        sys.stdout.flush()
    #print("") #skip line
    #Return the fitness as the mean of all episode scores
    if len(test_scores) > 0:
        fitness = test_scores.mean()
        #fitness = test_scores.sum() #Mean calculated by previous weight multiplier
    #sys.stdout.write("    Reward: %.1f mean: %.1f±%.1f, min: %.1f, max: %.1f\n" % (
    #fitness,test_scores.mean(), test_scores.std(), test_scores.min(),test_scores.max()))

    return (fitness,test_scores)

def performImgVerify():
    if 1:#Skip this now
        #This section means that each run of the generations was verified to be identical starting images
        #(so each episode was identical for each population)
        founddiff = -10
        firstimg = None
        getfirstimg = True
        print("PROCESSING IMAGE COMPARE")
        if len(allimg) <= 1:
            print("  Not enough genomes to compare")
        else:
            for img in allimg:
                if getfirstimg:
                    firstimg = img
                    getfirstimg = False
                    continue
                sys.stdout.write("      Verifying %d against %d..." % (img,firstimg))
                sys.stdout.flush()
                for ii in range(len(allimg[img])):
                    for jj in range(len(allimg[img][ii])):
                        #sys.stdout.write(".")
                        if allimg[firstimg][ii][jj] != allimg[img][ii][jj]:
                            founddiff = img
                            break
                    if founddiff > -10:
                        break
                if founddiff > -10:
                    break
                sys.stdout.write("\n")
            if founddiff > -10:
                sys.stdout.write("Found diff between initial run and re-run %d\n" %(founddiff))
            else:
                print("      Identical start images between populations")

    if 1:#Skip this now
        #This section means that each run of the generations was verified to be different episodes by looking at starting images
        #(so each episode was different for each population)
        founddiff = -1
        print("num images %d" % (len(allimg)))
        print("CHECK IF EPISODES ALL SAME?")
        if len(allimg)>0 and len(allimg[list(allimg.keys())[0]]) <= 1:
            print("  Not enough episodes to compare")
        else:
            for img in allimg:
                sys.stdout.flush()
                firstimg = 0
                for ii in range(1,len(allimg[img])):
                    sys.stdout.write("      Verifying pop %d episode %d against %d..." % (img,ii,firstimg))
                    issame = True
                    numdiff = 0
                    for jj in range(len(allimg[img][ii])):
                        #sys.stdout.write(".")
                        if allimg[img][firstimg][jj] != allimg[img][ii][jj]:
                            issame = False
                            numdiff += 1
                    if issame:
                        print("      Found same between 0 episode and next episodes on genome %d" %(img))
                        break
                    else:
                        print("      Found %d image differences"%(numdiff))
                sys.stdout.write("\n")


def watchreplay(genomenum=0,replayfiles=None,num_episodes=episodes_to_watch):
    #Replay episodes
    import vizdoom
    mygame.startgame(watch=True)
    mygame.game.close()
    mygame.game.set_window_visible(True)
    mygame.game.set_render_hud(True)
    mygame.game.set_render_weapon(True)
    mygame.game.set_mode(vizdoom.Mode.ASYNC_PLAYER)
    mygame.game.init()
    test_scores = []
    fitness = 0
    if replayfiles:
        for replay in replayfiles:
            print("Starting replay file %s" %(replay))
            if not os.path.exists(replay):
                print(" File does not exist!")
                continue

            # Replays episodes stored in given file. Sending game command will interrupt playback.
            mygame.game.replay_episode(replay)

            while not mygame.game.is_episode_finished():
                # Use advance_action instead of make_action.
                mygame.game.advance_action(1)

            #Square the score after offset [to make 0 worst score]; then add weight depending on episode
            r = ((mygame.game.get_total_reward()+score_offset)**2)# * episode_fitness_weight[test_episode] #weight the score
            test_scores.append(r)

            #Wait between episodes
            time.sleep(1.0)
    else:
        for test_episode in range(num_episodes):
            playbackfile = 'tmp/tmpgame%d_%d.lmp' %(genomenum,test_episode)
            print("Starting episode %d with file %s" %(test_episode,playbackfile))
            if not os.path.exists(playbackfile):
                print(" File does not exist!")
                continue
            # Replays episodes stored in given file. Sending game command will interrupt playback.
            mygame.game.replay_episode(playbackfile)

            while not mygame.game.is_episode_finished():
                # Use advance_action instead of make_action.
                mygame.game.advance_action(1)

            #Square the score after offset [to make 0 worst score]; then add weight depending on episode
            r = ((mygame.game.get_total_reward()+score_offset)**2)# * episode_fitness_weight[test_episode] #weight the score
            test_scores.append(r)

            #Wait between episodes
            time.sleep(1.0)

    test_scores = np.array(test_scores)
    if len(test_scores) > 0:
        fitness = test_scores.mean()
    return (fitness,test_scores)

def watchgame(mygame,genomenum):
    output = watchreplay(genomenum,num_episodes=episodes_to_watch)
    #mygame.startgame(watch=True)
    #output = evaluate(net,realtime=True,num_episodes=episodes_to_watch)
    mygame.game.close()
    return output


def doTraining(initgenome=None,initpop=None):
    neat = None

    def runTesting(net,seed,dolearn,record):
        return evaluate(net,realtime=False,num_episodes=test_episodes_per_epoch,seed=seed,dolearn=dolearn,record=record)[0]

    mygame.startgame()
    #Set parameters based on vizdoom
    print("Starting the training!")
    print(" Number of episodes to train: %d" % (test_episodes_per_epoch))
    neat_params['num_inputs'] = mygame.getStateSize() #Set from screen size
    neat_params['num_outputs'] = len(mygame.actions)  #Set actions
    neat = MyNEAT(neat_params,runTesting,initgenome=initgenome,initpop=initpop)

    time_start = time.time()
    bestgen = neat.train()
    print("Total elapsed time: %.2f minutes" % ((time.time() - time_start) / 60.0))
    #Finished Learning
    mygame.game.close()
    return bestgen

def doWatching(bestgen):
    print("Watching %d episodes" %(episodes_to_watch))
    (fitness,test_scores) = watchgame(mygame,bestgen)
    print("Final score %.1f: mean: %.1f±%.1f," % (
    fitness,test_scores.mean(), test_scores.std()), "min: %.1f" % test_scores.min(), "max: %.1f" % test_scores.max())

def doWatchingNet(net):
    #First create replays (will default to genome ID of -1
    mygame.startgame()
    evaluate(net,realtime=True,num_episodes=episodes_to_watch)
    (fitness,test_scores) = watchgame(mygame,-1) #-1 is the genome ID for file
    print("Final score %.1f: mean: %.1f±%.1f," % (
    fitness,test_scores.mean(), test_scores.std()), "min: %.1f" % test_scores.min(), "max: %.1f" % test_scores.max())


def saveNet(net,filename=save_best_net_file):
    sys.stdout.write("Saving the best neural network to %s..." %(filename))
    sys.stdout.flush()
    net.save(filename)
    with open(filename+'.depth','w') as f:
        f.write(str(net.depth))
    sys.stdout.write("done\n")
    sys.stdout.flush()

def loadNet(filename=save_best_net_file):
    sys.stdout.write("Loading neural network from %s..." %(filename))
    sys.stdout.flush()
    net = GenomeRun(filename=filename)
    with open(filename+'.depth','r') as f:
        net.depth = int(f.read())
    sys.stdout.write("done\n")
    sys.stdout.flush()
    return net

def saveGenome(net,filename=save_best_genome_file):
    sys.stdout.write("Saving the best genome to %s..." %(filename))
    sys.stdout.flush()
    net.Save(filename)
    sys.stdout.write("done\n")
    sys.stdout.flush()

def loadGenome(filename=save_best_genome_file):
    import MultiNEAT as NEAT
    sys.stdout.write("Loading genome from %s..." %(filename))
    sys.stdout.flush()
    net = NEAT.Genome(filename)
    sys.stdout.write("done\n")
    sys.stdout.flush()
    return net


def savePop(net,filename=save_best_pop_file):
    sys.stdout.write("Saving the best population to %s..." %(filename))
    sys.stdout.flush()
    net.Save(filename)
    sys.stdout.write("done\n")
    sys.stdout.flush()

def loadPop(filename=save_best_pop_file):
    import MultiNEAT as NEAT
    sys.stdout.write("Loading population from %s..." %(filename))
    sys.stdout.flush()
    net = NEAT.Population(filename)
    sys.stdout.write("done\n")
    sys.stdout.flush()
    return net



if __name__ == '__main__':
    import argparse
    #=======================
    # Process arguments
    parser = argparse.ArgumentParser(description='Demo NEAT.')
    parser.add_argument('--load_genome', dest='genome', metavar='FILE',
                        help='File of a dumped genome')
    parser.add_argument('--load_net', dest='net', metavar='FILE',
                        help='File of a dumped neural network')
    parser.add_argument('--gen', type=int, dest='num_gen', metavar='NUM',
                        help='Number of epochs/generations to train a population group.')
    parser.add_argument('--train_episodes', type=int, dest='num_episodes', metavar='NUM',
                        help='Number of episodes to train.')
    parser.add_argument('--watch_episodes', type=int, dest='num_episodes_watch', metavar='NUM',
                        help='Number of episodes to watch.')
    parser.add_argument('--train', type=int, dest='num_train', metavar='NUM',
                        help='Number of population groups to train.')
    parser.add_argument('--pop', type=int, dest='num_pop', metavar='NUM',
                        help='Size of the population for a group.')
    parser.add_argument('--load_pop', dest='pop', metavar='FILE',
                        help='File of a dumped population')
    parser.add_argument('--prefix', dest='prefix', metavar='PREFIX',
                        help='Prefix to add to all dumped files')
    parser.add_argument('--type', dest='type', metavar='TYPE',
                        help='Type of NEAT to use (e.g. NEAT or ESHyperNEAT)')
    parser.add_argument('--replay', dest='replay', metavar='FILES',
                        help='Comma separated list of replay files')
    parser.add_argument('--learn', dest='learn', action='store_true', default=False,
                        help='Make it learn while executing')
    parser.add_argument('--last_gen', dest='lastgen', type=int, metavar='NUM',
                        help='Last generation executed')
    lastgen = 0
    prefix = "best"
    neattype = 'NEAT'
    args = parser.parse_args()
    if args.prefix:
        prefix = args.prefix
    if args.type:
        if args.type not in NEAT_types:
            print("--type is not in NEAT types")
            parser.print_help()
            exit(1)
        neattype = args.type
    neat_params['prefix'] = prefix
    neat_params['type'] = neattype
    neat_params['learn'] = args.learn
    if args.learn:
            print("Using learning")

    if args.num_episodes:
        test_episodes_per_epoch = args.num_episodes
        episodes_to_watch = test_episodes_per_epoch
    if args.num_episodes_watch:
        episodes_to_watch = args.num_episodes_watch
    neat_params['num_episodes'] = test_episodes_per_epoch   #wait until it changes
    #If net set then just show demo
    if args.net:
        net = loadNet(args.net)
        print("Using neural network loaded to watch")
        doWatchingNet(net)
        exit(0)

    if args.replay:
        #Just do a replay and exist
        print("Just watching replays: %s"%(args.replay))
        watchreplay(replayfiles=args.replay.split(","),num_episodes=episodes_to_watch)
        exit(0)

    if args.lastgen:
        lastgen = args.lastgen
    neat_params['lastgen'] = lastgen
    initgenome = None
    initpop = None
    if args.genome:
        initgenome = loadGenome(args.genome)
    if args.pop:
        initpop = loadPop(args.pop)
    if args.num_gen:
        neat_params['num_generations'] = args.num_gen #Number of epochs to train a population group
    if args.num_train:
        neat_params['num_trainings'] = args.num_train #Number of population groups to train
    if args.num_pop:
        neat_params['population_size'] = args.num_pop #Size of population


    #=======================
    # Do training
    best = doTraining(initgenome=initgenome,initpop=initpop)

    #=======================
    # Save data
    print("======================================")
    import os
    if os.path.exists(prefix+save_best_net_file):
        os.remove(prefix+save_best_net_file)
    saveNet(best['net'],prefix+save_best_net_file)
    if os.path.exists(prefix+save_best_genome_file):
        os.remove(prefix+save_best_genome_file)
    saveGenome(best['genome'], prefix+save_best_genome_file)
    if os.path.exists(prefix+save_best_pop_file):
        os.remove(prefix+save_best_pop_file)
    savePop(best['pop'],prefix+save_best_pop_file)


    #=======================
    # Watch best network
    print("======================================")
    print("Training finished. Verify different episodes but same starts for all population!")
    if CONST_VERIFY_IMAGES:
        performImgVerify()
    print("It's time to watch!")
    doWatching(best['popnum'])
